<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rodller
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
    <?php
    // Getting main header layout
    $header_layout = rodller_get_option("header_layout");
    ?>
    <header id="rodller-main-header" class="rodller-site-header" role="banner">
        <?php
        // Getting top bar
        if (rodller_get_option("top_bar")) {
            get_template_part('template-parts/header/top-bar');
        }
        ?>
        <div id="rodller-header-layout" class="rodller-header-<?php echo esc_attr("layout-" . $header_layout) ?>">
            <?php get_template_part('template-parts/header/layout-' . $header_layout); ?>
        </div>

        <?php if ( rodller_get_option( 'sticky_header' )): ?>
            <?php get_template_part( 'template-parts/header/sticky' ); ?>
        <?php endif; ?>
        
    </header><!-- #rodller-main-header -->
    
	<?php get_template_part( 'template-parts/header/responsive' ); ?>
